filetype off
call pathogen#runtime_append_all_bundles()
filetype plugin indent on

set nocompatible

set modelines=0

set tabstop=4
set shiftwidth=4
set softtabstop=4
set expandtab
" gui options
set go-=m
set go-=T
set go-=r
" no visual bell
set vb t_vb=

" Basic options
set encoding=utf-8
set scrolloff=3
set autoindent
set showmode
set showcmd
set hidden
set wildmenu
set wildmode=list:longest
set cursorline
set ttyfast
set ruler
set number
set backspace=indent,eol,start
"set relativenumber
set laststatus=2
"set undofile
"set undoreload=10000

" Copy/Paste
nmap <C-V> "+gP
imap <C-V> <ESC><C-V>i
vmap <C-C> "+y

set t_Co=256
"colorscheme wombat256mod
let g:solarized_termcolors=256
" set background=light
set background=dark
colorscheme solarized


" run file with PHP CLI (CTRL-M)
autocmd FileType php noremap <C-M> :w!<CR>:!/usr/bin/php %<CR>
set noswapfile

" php-doc plugin
inoremap <C-P> <ESC>:call PhpDocSingle()<CR>i
nnoremap <C-P> :call PhpDocSingle()<CR>
vnoremap <C-P> :call PhpDocRange()<CR> 
let g:pdv_cfg_Author = "Aboozar Ghaffari <me@tartan.pro>"
let g:pdv_cfg_Copyright = "2014 Tartan"
let g:pdv_cfg_Package = "Tartan Package"
let g:pdv_cfg_License = ""
let g:pdv_cfg_Version = "0.0.1" 

"omni completion
autocmd FileType php set omnifunc=phpcomplete#CompletePHP

let Tlist_Show_One_File = 1
map <F7> :TlistToggle<CR>
map <F2> :NERDTreeToggle<CR>

" tabs
nmap <C-j> <C-PageUp> 
nmap <C-k> <C-PageDown> 
map <C-t> :tabnew<CR>
map <C-c> :tabclose<CR>
